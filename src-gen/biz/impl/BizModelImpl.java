/**
 */
package biz.impl;

import biz.BizModel;
import biz.BizPackage;
import biz.Customer;
import biz.Invoice;
import biz.Product;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link biz.impl.BizModelImpl#getCustomer <em>Customer</em>}</li>
 *   <li>{@link biz.impl.BizModelImpl#getInvoice <em>Invoice</em>}</li>
 *   <li>{@link biz.impl.BizModelImpl#getProduct <em>Product</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BizModelImpl extends MinimalEObjectImpl.Container implements BizModel {
	/**
	 * The cached value of the '{@link #getCustomer() <em>Customer</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomer()
	 * @generated
	 * @ordered
	 */
	protected EList<Customer> customer;

	/**
	 * The cached value of the '{@link #getInvoice() <em>Invoice</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInvoice()
	 * @generated
	 * @ordered
	 */
	protected EList<Invoice> invoice;

	/**
	 * The cached value of the '{@link #getProduct() <em>Product</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProduct()
	 * @generated
	 * @ordered
	 */
	protected EList<Product> product;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BizModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BizPackage.Literals.BIZ_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Customer> getCustomer() {
		if (customer == null) {
			customer = new EObjectContainmentEList<Customer>(Customer.class, this, BizPackage.BIZ_MODEL__CUSTOMER);
		}
		return customer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Invoice> getInvoice() {
		if (invoice == null) {
			invoice = new EObjectContainmentEList<Invoice>(Invoice.class, this, BizPackage.BIZ_MODEL__INVOICE);
		}
		return invoice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Product> getProduct() {
		if (product == null) {
			product = new EObjectContainmentEList<Product>(Product.class, this, BizPackage.BIZ_MODEL__PRODUCT);
		}
		return product;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case BizPackage.BIZ_MODEL__CUSTOMER:
			return ((InternalEList<?>) getCustomer()).basicRemove(otherEnd, msgs);
		case BizPackage.BIZ_MODEL__INVOICE:
			return ((InternalEList<?>) getInvoice()).basicRemove(otherEnd, msgs);
		case BizPackage.BIZ_MODEL__PRODUCT:
			return ((InternalEList<?>) getProduct()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case BizPackage.BIZ_MODEL__CUSTOMER:
			return getCustomer();
		case BizPackage.BIZ_MODEL__INVOICE:
			return getInvoice();
		case BizPackage.BIZ_MODEL__PRODUCT:
			return getProduct();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case BizPackage.BIZ_MODEL__CUSTOMER:
			getCustomer().clear();
			getCustomer().addAll((Collection<? extends Customer>) newValue);
			return;
		case BizPackage.BIZ_MODEL__INVOICE:
			getInvoice().clear();
			getInvoice().addAll((Collection<? extends Invoice>) newValue);
			return;
		case BizPackage.BIZ_MODEL__PRODUCT:
			getProduct().clear();
			getProduct().addAll((Collection<? extends Product>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case BizPackage.BIZ_MODEL__CUSTOMER:
			getCustomer().clear();
			return;
		case BizPackage.BIZ_MODEL__INVOICE:
			getInvoice().clear();
			return;
		case BizPackage.BIZ_MODEL__PRODUCT:
			getProduct().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case BizPackage.BIZ_MODEL__CUSTOMER:
			return customer != null && !customer.isEmpty();
		case BizPackage.BIZ_MODEL__INVOICE:
			return invoice != null && !invoice.isEmpty();
		case BizPackage.BIZ_MODEL__PRODUCT:
			return product != null && !product.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //BizModelImpl
