/**
 */
package biz;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Customer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link biz.Customer#getInvoice <em>Invoice</em>}</li>
 *   <li>{@link biz.Customer#getName <em>Name</em>}</li>
 *   <li>{@link biz.Customer#getCustomerID <em>Customer ID</em>}</li>
 * </ul>
 *
 * @see biz.BizPackage#getCustomer()
 * @model
 * @generated
 */
public interface Customer extends EObject {
	/**
	 * Returns the value of the '<em><b>Invoice</b></em>' reference list.
	 * The list contents are of type {@link biz.Invoice}.
	 * It is bidirectional and its opposite is '{@link biz.Invoice#getCustomer <em>Customer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Invoice</em>' reference list.
	 * @see biz.BizPackage#getCustomer_Invoice()
	 * @see biz.Invoice#getCustomer
	 * @model opposite="customer"
	 * @generated
	 */
	EList<Invoice> getInvoice();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see biz.BizPackage#getCustomer_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link biz.Customer#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Customer ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Customer ID</em>' attribute.
	 * @see #setCustomerID(int)
	 * @see biz.BizPackage#getCustomer_CustomerID()
	 * @model id="true"
	 * @generated
	 */
	int getCustomerID();

	/**
	 * Sets the value of the '{@link biz.Customer#getCustomerID <em>Customer ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Customer ID</em>' attribute.
	 * @see #getCustomerID()
	 * @generated
	 */
	void setCustomerID(int value);

} // Customer
