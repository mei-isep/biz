/**
 */
package biz;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link biz.BizModel#getCustomer <em>Customer</em>}</li>
 *   <li>{@link biz.BizModel#getInvoice <em>Invoice</em>}</li>
 *   <li>{@link biz.BizModel#getProduct <em>Product</em>}</li>
 * </ul>
 *
 * @see biz.BizPackage#getBizModel()
 * @model
 * @generated
 */
public interface BizModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Customer</b></em>' containment reference list.
	 * The list contents are of type {@link biz.Customer}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Customer</em>' containment reference list.
	 * @see biz.BizPackage#getBizModel_Customer()
	 * @model containment="true"
	 * @generated
	 */
	EList<Customer> getCustomer();

	/**
	 * Returns the value of the '<em><b>Invoice</b></em>' containment reference list.
	 * The list contents are of type {@link biz.Invoice}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Invoice</em>' containment reference list.
	 * @see biz.BizPackage#getBizModel_Invoice()
	 * @model containment="true"
	 * @generated
	 */
	EList<Invoice> getInvoice();

	/**
	 * Returns the value of the '<em><b>Product</b></em>' containment reference list.
	 * The list contents are of type {@link biz.Product}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Product</em>' containment reference list.
	 * @see biz.BizPackage#getBizModel_Product()
	 * @model containment="true"
	 * @generated
	 */
	EList<Product> getProduct();

} // BizModel
