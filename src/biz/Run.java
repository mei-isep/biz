package biz;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class Run {
	
	private static void writeModel() {
		//--------------------------------------------------------
		// Programmatically editing the model instance...
		BizFactory factory = BizFactory.eINSTANCE;

		BizModel model = factory.createBizModel();

		// Customer 1
		Customer customer = factory.createCustomer();
		customer.setName("Alexandre Braganca");
		
		model.getCustomer().add(customer);

		// Customer 2
		Customer customer2 = factory.createCustomer();
		customer2.setName("Joao Mendes");
		
		model.getCustomer().add(customer2);
		
		//--------------------------------------------------------
		// Saving the model...
		// Create a resource set.
		ResourceSet resourceSet = new ResourceSetImpl();

		// Register the default resource factory -- only needed for stand-alone, i.e, running outside eclipse!
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());

		  // Get the URI of the model file.
		  URI fileURI = URI.createFileURI(new File("instances/mybiz.xmi").getAbsolutePath());

		  // Create a resource for this file.
		  Resource resource = resourceSet.createResource(fileURI);

		  // Add the book and writer objects to the contents.
		  resource.getContents().add(model);

		  // Save the contents of the resource to the file system.
		  try
		  {
		    resource.save(Collections.EMPTY_MAP);
		  }
		  catch (IOException e) {}			
	}
	
	private static void readModel() {
		//--------------------------------------------------------
		// Loading the model...
		// Create a resource set.
		 ResourceSet resourceSet2 = new ResourceSetImpl();

		  // Register the default resource factory -- only needed for stand-alone!
		  resourceSet2.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());

		  // Register the package -- only needed for stand-alone!
		  BizPackage libraryPackage = BizPackage.eINSTANCE;

		   // Get the URI of the model file.
		   URI fileURI2 = URI.createFileURI(new File("instances/mybiz.xmi").getAbsolutePath());

		   // Demand load the resource for this file.
		   Resource resource2 = resourceSet2.getResource(fileURI2, true);

		   // Print the contents of the resource to System.out.
		   try
		   {
		     resource2.save(System.out, Collections.EMPTY_MAP);
		   }
		   catch (IOException e) {}		 
		   
		   // Accessing the elements in the Model...
		   // resource2.
		   for (EObject obj:resource2.getContents()) {
			   if (BizModel.class.isInstance(obj)) {
				   BizModel bizModel=(BizModel)obj;
				   
				   System.out.println("Model found:");
				   for (Customer c:bizModel.getCustomer()) {
					   System.out.println(" >Customer: "+c.getName());
				   }
			   }
		   }		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//writeModel();
		
		readModel();
	}

}
